import classes from './IndexPage.module.scss';

import { useContext } from 'react';
import { useEthers } from '@usedapp/core';

import { UserDataContext } from '../../App';
import { UserDataContextType } from '../../models/Participation';

import Intro from './components/Intro';
import RegistrationForm from './components/RegistrationForm';
import ParticipationList from './components/ParticipationList';

const IndexPage = () => {
	const { account } = useEthers();

	const {
		userData,
		setUserData,
	} = useContext(UserDataContext) as UserDataContextType;

	const registrationFormSubmitHandler = (
		name: string,
		email: string,
	) => {
		setUserData({
			id: Date.now().toString(),
			username: name,
			email: email,
			address: account || '',
		});
	};

	const deleteUserData = () => {
		setUserData(null);
	};

	return (
		<main className={classes.indexPage}>
			<div className="container">
				<div className={classes.indexPage__content}>
					<div className={classes.indexPage__intro}>
						<Intro />
					</div>

					<div className={classes.indexPage__registration}>
						<RegistrationForm
							userData={userData}
							onSubmit={registrationFormSubmitHandler}
						/>
					</div>

					{account && (
						<div className={classes.indexPage__participationList}>
							<ParticipationList
								userData={userData}
								onDelete={deleteUserData}
							/>
						</div>
					)}
				</div>
			</div>
		</main >
	);
};

export default IndexPage;
