import './index.scss';

import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';

import { BrowserRouter } from 'react-router-dom';

import {
	Config,
	Mainnet,
	DAppProvider,
} from '@usedapp/core'

import { getDefaultProvider } from 'ethers'

const config: Config = {
	readOnlyChainId: Mainnet.chainId,
	readOnlyUrls: {
		[Mainnet.chainId]: getDefaultProvider('mainnet'),
	},
}

const root = ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement
);

root.render(
	<BrowserRouter>
		<React.StrictMode>
			<DAppProvider config={config}>
				<App />
			</DAppProvider>
		</React.StrictMode>
	</BrowserRouter>
);
