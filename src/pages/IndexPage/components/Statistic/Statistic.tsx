import classes from './Statistic.module.scss';

import CountUp from 'react-countup';

interface StatisticItem {
	id: number;
	text: string;
	value: number;
}

const statisticItems: StatisticItem[] = [
	{
		id: 1,
		text: 'Lorem ipsum dolor',
		value: 12345,
	},
	{
		id: 2,
		text: 'Lorem ipsum dolor',
		value: 12345,
	},
	{
		id: 3,
		text: 'Lorem ipsum dolor',
		value: 12345,
	},
];

const Statistic = () => {
	return (
		<div className={classes.statistic}>
			<p className={classes.statistic__title}>
				Roadmap stats
			</p>

			<ul className={classes.statistic__list}>
				{statisticItems.map((statisticItem) => (
					<li
						key={statisticItem.id}
						className={classes.statistic__item}
					>
						<p className={classes.statistic__name}>
							<CountUp
								end={statisticItem.value}
								separator=", "
							/>
						</p>

						<p className={classes.statistic__value}>
							{statisticItem.text}
						</p>
					</li>
				))}
			</ul>
		</div>
	);
};

export default Statistic;
