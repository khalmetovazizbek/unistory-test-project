import classes from './NotificationModal.module.scss';

import Modal from '../UI/Modal';
import Button from '../UI/Button';

interface NotificationModalProps {
	open: boolean;
	onClose: () => void;
}

const NotificationModal = ({
	open,
	onClose,
}: NotificationModalProps) => {
	return (
		<Modal
			open={open}
			onClose={onClose}
		>
			<div className={classes.notificationModal}>
				<p className={classes.notificationModal__title}>
					metamask extention
				</p>

				<p className={classes.notificationModal__description}>
					To work with our application, you have to install the &nbsp;
					<a
						href="https://metamask.io/download/"
						target="_blank"
						rel="noreferrer"
						className={classes.notificationModal__link}
					>
						Metamask browser extension
					</a>
				</p>

				<Button onClick={onClose}>
					Skip this step
				</Button>
			</div>
		</Modal>
	);
};

export default NotificationModal;
