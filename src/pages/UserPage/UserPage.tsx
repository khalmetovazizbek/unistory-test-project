import classes from './UserPage.module.scss';

import useSWR from 'swr';

import { useParams } from "react-router-dom";

const fetcher = (url: string) => fetch(url).then((res) => res.json());

const UserPage = () => {
	const { userId } = useParams();

	const { data, isLoading } = useSWR(
		`https://new-backend.unistory.app/api/data/id/${userId}`,
		fetcher,
	);

	return (
		<main className={classes.userPage}>
			<div className="container">
				<div className={classes.userPage__content}>
					<h1 className={classes.userPage__title}>
						Personal data
					</h1>

					{isLoading ? (
						<p>
							loading
						</p>
					) : (
						<div className={classes.userPage__info}>
							<div className={classes.userPage__infoItem}>
								<p className={classes.userPage__name}>
									name
								</p>
								<p className={classes.userPage__value}>
									{data?.username}
								</p>
							</div>
							<div className={classes.userPage__infoItem}>
								<p className={classes.userPage__name}>
									email
								</p>
								<p className={classes.userPage__value}>
									{data?.email}
								</p>
							</div>
							<div className={classes.userPage__infoItem}>
								<p className={classes.userPage__name}>
									wallet
								</p>
								<p className={classes.userPage__value}>
									{data?.address}
								</p>
							</div>
						</div>
					)}

					<div className={classes.planet} >
						<img
							src="/images/planet.png"
							alt="planet"
							className={classes.planet__image}
						/>

						<div className={classes.planet__inner} />

						<div className={classes.planet__progress}>
							<span className={classes.planet__dot} />
							<span className={classes.planet__dot} />
						</div>
					</div>
				</div>
			</div>
		</main>
	);
};

export default UserPage;
