import classes from './ParticipationList.module.scss';

import useSWR from 'swr';

import {
	useState,
	useEffect,
} from 'react';

import { Link } from 'react-router-dom';

import { useInView } from 'react-intersection-observer';

import Participation from '../../../../models/Participation';

const fetcher = (url: string) => fetch(url).then((res) => res.json());

interface MetaData {
	perPage: number;
	currentPage: number;
	totalPages: number;
}

interface ParticipationListProps {
	userData: Participation | null;
	onDelete: () => void;
}

const ParticipationList = ({
	userData,
	onDelete,
}: ParticipationListProps) => {
	const [participations, setParticipations] = useState<Participation[]>([]);

	const [page, setPage] = useState<number>(0);
	const [limit, setLimit] = useState<number>(20);
	const [metaData, setMetaData] = useState<MetaData | null>(null);

	const { ref, inView } = useInView({
		threshold: 0,
	});

	const { data, isLoading } = useSWR(
		`https://new-backend.unistory.app/api/data?page=${page}&perPage=${limit}`,
		fetcher
	);

	useEffect(() => {
		return () => setParticipations([]);
	}, []);

	useEffect(() => {
		if (
			inView &&
			!isLoading &&
			metaData &&
			page < metaData.totalPages
		) {
			setPage((state) => (state + 1));
		}
	}, [inView]);

	useEffect(() => {
		if (data) {
			if (data.items) {
				setParticipations((state) => [...state, ...data.items]);
			}

			if (data.meta) {
				setMetaData(data.meta);
			}
		}
	}, [data]);

	useEffect(() => {
		if (
			page === 0 &&
			data?.items &&
			participations.length === 0
		) {
			setParticipations(data.items);
		} else {
			setPage(0);
		}
	}, [userData]);

	if (!userData?.id) {
		return null;
	}

	return (
		<div className={classes.participationList}>
			<h3 className={classes.participationList__title}>
				Participation listing (enable only for participants)
			</h3>

			<div className={classes.participationList__table}>
				<div className={classes.table}>
					<div className={classes.table__header}>
						<p className={classes.table__label}>
							name
						</p>
						<p className={classes.table__label}>
							email
						</p>
						<p className={classes.table__label}>
							wallet
						</p>
					</div>

					<ul className={classes.table__body}>
						<li
							key={userData.id}
							className={classes.table__row}
						>
							<div className={[
								classes.table__item,
								classes.table__item_active,
							].join(' ')}>
								<p>
									{userData?.username}
								</p>
								<p>
									{userData?.email}
								</p>
								<p>
									{userData?.address?.slice(0, 19)}...
								</p>
								<button
									className={classes.table__delete}
									onClick={() => {
										onDelete();
										setParticipations([]);
									}}
								/>
							</div>
						</li>

						{participations.length ? participations.map((participation, index) => (
							<li
								key={participation?.id}
								className={classes.table__row}
								ref={index === (participations.length - 3) ? ref : null}
							>
								<Link
									to={`/${participation?.id}`}
									className={classes.table__item}
								>
									<p>
										{participation?.username}
									</p>
									<p>
										{participation?.email}
									</p>
									<p>
										{participation?.address?.slice(0, 19)}...
									</p>
								</Link>
							</li>
						)) : null}
					</ul>
				</div>
			</div>
		</div>
	);
};

export default ParticipationList;
