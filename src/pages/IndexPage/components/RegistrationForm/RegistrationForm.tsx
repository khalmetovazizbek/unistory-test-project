import classes from './RegistrationForm.module.scss';

import { useRef } from 'react';
import { useEthers } from '@usedapp/core';

import Participation from '../../../../models/Participation';

import Button from '../../../../components/UI/Button';

interface RegistrationFormProps {
	userData: Participation | null;
	onSubmit: (name: string, email: string) => void;
}

const RegistrationForm = ({
	userData,
	onSubmit,
}: RegistrationFormProps) => {
	const { account } = useEthers();

	const nameInput = useRef<HTMLInputElement>(null);
	const emailInput = useRef<HTMLInputElement>(null);

	return (
		<div className={classes.registrationForm}>
			<h3 className={classes.registrationForm__title}>
				Beta test registration
			</h3>

			<p className={classes.registrationForm__description}>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</p>

			<form
				className={classes.registrationForm__form}
				onSubmit={(event) => {
					event.preventDefault();
					event.stopPropagation();

					onSubmit(
						nameInput?.current?.value || '',
						emailInput?.current?.value || '',
					);
				}}
			>
				<div className={classes.registrationForm__inputField}>
					<label
						htmlFor="name"
						className={classes.registrationForm__label}
					>
						name
					</label>

					{userData?.username ? (
						<p className={classes.registrationForm__text}>
							{userData.username}
						</p>
					) : (
						<input
							id="name"
							type="text"
							required
							autoComplete="off"
							placeholder="We will display your name in participation list"
							ref={nameInput}
							className={classes.registrationForm__input}
						/>
					)}
				</div>

				<div className={classes.registrationForm__inputField}>
					<label
						htmlFor="email"
						className={classes.registrationForm__label}
					>
						email
					</label>

					{userData?.email ? (
						<p className={classes.registrationForm__text}>
							{userData.email}
						</p>
					) : (
						<input
							id="email"
							type="email"
							required
							autoComplete="off"
							placeholder="We will display your email in participation list"
							ref={emailInput}
							className={classes.registrationForm__input}
						/>
					)}
				</div>

				{userData ? (
					<Button disabled>
						Get early access
					</Button>
				) : (
					<Button disabled={!account}>
						List me to the table
					</Button>
				)}
			</form>
		</div>
	);
};

export default RegistrationForm;
