import './App.scss';

import { useState, createContext } from 'react';
import { Route, Routes } from 'react-router-dom';

import Participation, {
	UserDataContextType,
} from './models/Participation';

import IndexPage from './pages/IndexPage';
import UserPage from './pages/UserPage';
import Header from './components/Header';
import NotificationModal from './components/NotificationModal';

export const UserDataContext = createContext<UserDataContextType | null>(null);

const App = () => {
	const [modalOpen, setModalOpen] = useState<boolean>(true);
	const [userData, setUserData] = useState<Participation | null>(null);

	return (
		<UserDataContext.Provider value={{ userData, setUserData }}>
			<div className="App">
				<Header />

				<NotificationModal
					open={modalOpen}
					onClose={() => setModalOpen(false)}
				/>

				<Routes>
					<Route
						path="/"
						element={<IndexPage />}
					/>
					<Route
						path="/:userId"
						element={<UserPage />}
					/>
				</Routes>
			</div>
		</UserDataContext.Provider>
	);
};

export default App;
