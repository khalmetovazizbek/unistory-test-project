import classes from './Intro.module.scss';

import { useState } from 'react';

import Statistic from '../Statistic';

const xCenter = 263;
const yCenter = 263;
const maxLength = 103;

const Intro = () => {
	const [pos, setPos] = useState({ x: 0, y: 0 });

	const onMouseMove = (event: any) => {
		let xPos = event.nativeEvent?.offsetX;
		let yPos = event.nativeEvent?.offsetY;

		const length = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));

		if (length <= maxLength) {
			setPos({
				x: xPos,
				y: yPos,
			});
		} else {
			const xDelda = xPos - xCenter;
			const yDelta = yPos - yCenter;

			const angle = Math.atan2(yDelta, xDelda);

			xPos = xCenter + maxLength * Math.cos(angle);
			yPos = yCenter + maxLength * Math.sin(angle)

			setPos({
				x: xPos,
				y: yPos,
			});
		}
	};

	const onMouseLeave = () => {
		setPos({
			x: 0,
			y: 0,
		});
	};

	return (
		<section className={classes.intro}>
			<div className={classes.intro__title}>
				<h1>
					Explore Your own planet
					<br />
					In <span>our New</span> metaverse
				</h1>

				<div
					className={classes.planet}
					onMouseMove={onMouseMove}
					onMouseLeave={onMouseLeave}
				>
					<img
						src="/images/planet.png"
						alt="planet"
						className={classes.planet__image}
						style={{
							transform: `translate3d(${pos.x || xCenter}px, ${pos.y || yCenter}px, 0px) translate3d(-50%, -50%, 0px)`,
						}}
					/>

					<div className={classes.planet__inner} />

					<div className={classes.planet__progress}>
						<div className={classes.planet__date}>
							<p>
								Q1 2022
							</p>

							<span />
						</div>

						<span className={classes.planet__dot} />
						<span className={classes.planet__dot} />
					</div>
				</div>
			</div>

			<p className={classes.intro__description}>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</p>

			<div className={classes.intro__statistic}>
				<Statistic />
			</div>
		</section>
	);
};

export default Intro;
