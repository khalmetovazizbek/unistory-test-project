export default interface Participation {
	id: string;
	email: string;
	address: string;
	username: string;
}

export type UserDataContextType = {
	userData: Participation | null;
	setUserData: (state: Participation | null) => void;
};