import classes from './Header.module.scss';

import { Link } from 'react-router-dom';
import { useEthers } from '@usedapp/core';

import Button from '../UI/Button';

const Header = () => {
	const { account, activateBrowserWallet } = useEthers();

	return (
		<header className={classes.header}>
			<div className="container">
				<div className={classes.header__content}>
					<Link to="/">
						<div className={classes.header__logo}>
							Logo
						</div>
					</Link>

					{account ? (
						<p className={classes.header__userAddress}>
							{account.slice(0, 19)}...
						</p>
					) : (
						<Button onClick={activateBrowserWallet}>
							Connect metamask
						</Button>
					)}
				</div>
			</div>
		</header>
	);
};

export default Header;
