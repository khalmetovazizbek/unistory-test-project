import classes from './Button.module.scss';

import { ReactNode } from 'react';

interface ButtonProps {
	onClick?: () => void;
	disabled?: boolean;
	children: ReactNode;
}

const Button = ({
	onClick,
	disabled = false,
	children,
}: ButtonProps) => {
	return (
		<button
			onClick={onClick}
			disabled={disabled}
			className={classes.button}
		>
			{children}
		</button>
	);
};

export default Button;
