import classes from './Modal.module.scss';

import { ReactNode } from 'react';

interface ModalProps {
	open: boolean;
	onClose: () => void;
	children: ReactNode;
}

const Modal = ({
	open,
	onClose,
	children,
}: ModalProps) => {
	if (!open) {
		return null;
	}

	return (
		<div
			className={classes.modal}
			onClick={onClose}
			onKeyUp={(e) => console.log(e)}
		>
			<div
				onClick={(event: React.MouseEvent<HTMLElement>) => {
					event.stopPropagation();
				}}
			>
				{children}
			</div>
		</div>
	);
};

export default Modal;
